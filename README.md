## Building / Testing

To build locally (you must use java 1.7 for compiling, though we produce 1.6 compatible classes):

```
mvn compile
```
To run the app:

```
mvn spring-boot:run
```
To run tests:

```
mvn test
```
