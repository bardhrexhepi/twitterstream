package com.twitter.hbc.example;

import com.fasterxml.jackson.core.io.JsonStringEncoder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.common.IOUtils;
import com.twitter.hbc.core.endpoint.*;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioFormat.Encoding;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.impl.client.HttpClients;

import com.twitter.hbc.core.*;
import com.twitter.hbc.core.event.*;

import twitter4j.TweetEntity;


import java.util.*;

/**
 * Created by o on 07.07.17.
 *
 */


public class UserStream{



    public static void main(String[] args) throws FileNotFoundException, IOException{
    	
        /** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
        BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);

        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
        //List<Long> followings = Lists.newArrayList(1234L, 566788L);
        List<Long> followings = Lists.newArrayList(1234L, 621543L, 747807250819981312L, 612473L,807095L);
        followings = Lists.newArrayList(873157602L);	
        //List<String> terms = Lists.newArrayList("twitter", "api");
        hosebirdEndpoint.followings(followings);
        //hosebirdEndpoint.trackTerms(terms);

		
		

		Authentication hosebirdAuth = new OAuth1("THDwPlDM9fxOGyGzavIsXP7sE", "OgCiKTgbntzw9FtPrFMrsHdFrBX8isWWqUI8LZh5PZphe5ZnzP", "1408927214-89IUKYGaQmMkCBtoQYYxsP8OY4481ZTqQigvOE2", "Dv5yJM38FUwJAwNOBTpzSWop5cseMsVljHYmgyWOH61lw");
        //Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, token, tokenSecret);
        
        //Creating a client
        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(hosebirdAuth)
                .endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue))
                .eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

        Client hosebirdClient = builder.build();
        // Attempts to establish a connection.
        hosebirdClient.connect();
        try{
            // on a different thread, or multiple different threads....
            while (!hosebirdClient.isDone()) {
                String msg = msgQueue.take();
                System.out.println(msg);

                         
                HttpClient httpclient = HttpClients.createDefault();
                HttpPost httppost = new HttpPost("http://localhost:8090/analyze/tweet");

                List<NameValuePair> params = new ArrayList<NameValuePair>(2);
                httppost.setEntity(new StringEntity(msg));

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
					InputStream instream = entity.getContent();
					instream.close();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
	

}
